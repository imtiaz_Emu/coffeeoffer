class HomeController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index

  end


  def offer
    invite = params['interest']
    date = params['date']
    place = params['place']
    CoffeeMailer.sample_email(invite, params['description'], date, place).deliver
    redirect_to root_path, notice: 'Thank You for your reply.'
  end
end

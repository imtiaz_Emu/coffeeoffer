class CoffeeMailer < ActionMailer::Base
  default from: "imtiaz.emu.it2@gmail.com"
  layout 'mailer'

  def sample_email(invite, description, date, place)
    @invitation = (invite == "0" ? 'Reject' : 'Accept')
    @des = description
    @date = date
    @place = place
    subject = "I " + @invitation + " Your coffee invitation!"
    mail(to: 'imtiaz.emu.it@gmail.com', subject: subject)
  end

end
